const path = require('path');
const express = require('express')
const app = express()

// for cookie
const cookieParser = require('cookie-parser')
app.use(cookieParser())

const mongoose = require('mongoose')

// for .env variables
const dotenv = require('dotenv')
dotenv.config()

// for rendering server side engine
const exphbs = require('express-handlebars')
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
})
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

// Routes
const authRoute = require('./routes/auth')

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname,'public')))

// Route middleware
app.use(authRoute)

async function start() {
    try{
        // mongo DB
        mongoose.connect(
            // process.env.DB_CONNECT, 
            "mongodb+srv://sadmin:strateg_in31@cluster0.v5hhv.mongodb.net/test_app",
            { useNewUrlParser: true },
            () => console.log('Connected to DB... OK!')
        )
        // Port server
        const PORT = process.env.PORT || 3000
        app.listen(PORT, () => console.log(`Server has been started on port ${PORT}... OK!`))
    }catch(e){
        console.log(e)
    }
}

start()