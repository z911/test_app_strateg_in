# strateg_in_test

Test technique
API sécurisée par Register/Login

## Nous vous demandons de développer une application permettant de s’inscrire via des informations basiques.

#### • Au premier accès, l’utilisateur doit se créer un compte (email, mdp) sur la route /register
#### • Une fois le compte créé, l’utilisateur doit utiliser la route /login pour récupérer un token.
#### • Une fois logué, l’utilisateur peut accéder à la liste des utilisateurs déjà enregistrés sur la plateforme via la route /users.

## Exigences

- [ ] [Langage : Node.js,](https://vladzolo.fr)
- [ ] [Normes et dépendances : ES6, fonctions fléchées, Express, Mongoose](https://vladzolo.fr)
- [ ] [DB : MongoDB (Possibilité de créer une DB gratuite via un compte Atlas)](https://vladzolo.fr)

## Donc j'ai utilisé:

- [ ] [Node.js, Express.js, Javascript,](https://vladzolo.fr)
- [ ] [Mongoose - pour base de données Mongodb,](https://vladzolo.fr)
- [ ] [express-handlebars - moteur de génération de page côté serveur,](https://vladzolo.fr)
- [ ] [bcryptjs - pour HASH mdp,](https://vladzolo.fr)
- [ ] [jsonwebtoken - pour token,](https://vladzolo.fr)
- [ ] [*/ @hapi/joi - pour validation des formulaires /!\ validation il faut finir au deuxième Sprint / .](https://vladzolo.fr)

### Mode development
```
npm run dev
```

### Server will start on port
```
http://localhost:3000/
```