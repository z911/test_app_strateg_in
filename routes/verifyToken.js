const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')


module.exports = function auth(req, res, next) {
    let token = req.header('authToken')
    token = req.cookies.authToken
   

    if(!token) return res.status(401).send('No access...')

    try{
        // const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        const verified = jwt.verify(token, 'qerqerqweadsfzx')
        req.user = verified
        next()
    }catch(e){
        res.status(400).send('Invalid token...')
    }
}