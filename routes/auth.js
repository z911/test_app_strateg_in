const router = require('express').Router()
const bcrypt = require('bcryptjs')
const jwt =require('jsonwebtoken')
const User = require('../model/User')
const dotenv = require('dotenv')
const verify = require('./verifyToken') 

const Joi = require('@hapi/joi')
const {registerValidation, loginValidation} = require('../validation')



// ** register **
router.post('/api/user/register', async(req, res) => {
    // Validation of data
    const {error} = registerValidation(req.body)
    if(error) return res.status(400).send(error.details[0].message);

    // check if user already in DB
    const isEmailExist = await User.findOne({email: req.body.email})
    if(isEmailExist) return res.status(400).send('Error: email already exist!');

    // res.send(req.body.email)

    // hash the password
    const salt = await bcrypt.genSalt(10) 
    const hashPassword = await bcrypt.hash(req.body.password, salt)

    // Create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashPassword
    })
    try {
        const savedUser = await user.save()
        // res.send(savedUser)
        res.redirect('/login');
    }catch(e){
        res.status(400).send(e)
    }
})

// ** login **
router.post('/api/user/login', async(req, res) => {
    // Validation of data
    const {error} = loginValidation(req.body)
    if(error) return res.status(400).send(error.details[0].message);

    // check if user in DB
    const isUserExist = await User.findOne({email: req.body.email})
    if(!isUserExist) return res.status(400).send('Error: email does not exist!');
    
    // check if password is correct
    const isValidPassword = await bcrypt.compare(req.body.password, isUserExist.password)
    if(!isValidPassword) return res.status(400).send('Error: password in not valid!');
    
    // create a token
    // const token = jwt.sign({_id: isUserExist._id}, process.env.TOKEN_SECRET)
    const token = jwt.sign({_id: isUserExist._id}, 'qerqerqweadsfzx')
    // res.header('authToken', token).send(token)
    // console.log('token: ',token)
    res.header('authToken', token)

    // for my test only 
    res.cookie('authToken', token);

    res.redirect('/api/user/users')

})

// ** users list **
router.get('/api/user/users', verify , async(req, res) => {
    const usersList = await User.find({})
    // console.log('usersList typeof: ', typeof usersList)
    // res.send(usersList)
    res.render('list', {
        title: 'Users list',
        isUsersList: true,
        usersList
    })

})

// index page
router.get('/', (req,res) => {
    console.log('index page ...')

    res.render('index', {
        title: 'About',
        isIndex: true
    })
})

// page create user
router.get('/register', (req,res) => {
    console.log('create user ...')

    res.render('create', {
        title: 'Create',
        isCreate: true
    })
})

//login page
router.get('/login', (req,res) => {
    console.log('login...')

    res.render('login', {
        title: 'Login',
        isLogin: true
    })
})

module.exports = router